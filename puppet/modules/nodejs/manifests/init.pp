class nodejs
{

  # install nodejs
  package { "nodejs":
    ensure => "present"
  }

  # install npm
  package { "npm":
    ensure => "present",
    require => Package["nodejs"]
  }

}
