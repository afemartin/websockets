Websockets
===============================================================================

Este proyecto consiste en crear un chat de video con NodeJS y WebSockets

*******************************************************************************

1. Configurar el entorno local de trabajo (Vagrant + Puppet)
-------------------------------------------------------------------------------

### Descargar herramientas:

Descarga e instala las aplicaciones compatibles con tu sistema operativo teniendo en cuenta si tu procesador es de 32 o 64 bits

* Vagrant (mínimo 1.7.2) https://www.vagrantup.com/downloads.html
* VirtualBox (mínimo 4.3.24) https://www.virtualbox.org/wiki/Downloads

NOTA: En Windows el fichero se encuentra en `C:\Windows\System32\Drivers\etc\hosts`

### Instalar Git

Para poder descargar el proyecto hay que instalar git en tu máquina física

Si al ejecutar el comando `git --version` en al consola nos indica que no reconoce el comando git tendremos que [instalarlo](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Descargar el proyecto

```bash
$ cd /path/to/workspace/
$ git clone git@github.com/afemartin/websockets.git
$ cd websockets/
$ vagrant up
```

### Configurar VirtualBox

Si al ejecutar `vagrant up` da este error:

> VirtualBox is complaining that the kernel module is not loaded. Please
> run `VBoxManage --version` or open the VirtualBox GUI to see the error
> message which should contain instructions on how to fix this error.

Ejecutar `VBoxManage --version` que mostrará una información como esta:

> WARNING: The vboxdrv kernel module is not loaded. Either there is no module
>          available for the current kernel (3.13.0-44-generic) or it failed to
>          load. Please recompile the kernel module and install it by
>            `sudo /etc/init.d/vboxdrv setup`
>          You will not be able to start VMs until this problem is fixed.
> 4.3.20r96996

Ejecutar `sudo /etc/init.d/vboxdrv setup` para recompilar el kernel que mostrará la siguiente salida:

> Stopping VirtualBox kernel modules ...done.
> Recompiling VirtualBox kernel modules ...done.
> Starting VirtualBox kernel modules ...done.

Después de esto ya podemos ejecutar de nuevo `vagrant up`

### Habilitar virtualización hardware

Si durante la ejecución de `vagrant up` se produce este error:

> ==> default: Waiting for machine to boot. This may take a few minutes...
> The guest machine entered an invalid state while waiting for it
> to boot. Valid states are 'starting, running'. The machine is in the
> 'poweroff' state. Please verify everything is configured
> properly and try again.
> If the provider you're using has a GUI that comes with it,
> it is often helpful to open that and watch the machine, since the
> GUI often has more helpful error messages than Vagrant can retrieve.
> For example, if you're using VirtualBox, run `vagrant up` while the
> VirtualBox GUI is open.

Tal como dicen [aquí](https://github.com/mitchellh/vagrant/issues/2157) lo mejor es intentar iniciar la máquina manualmente desde la GUI de VirtualBox que nos mostrará el siguiente error:

> Failed to open a session for the virtual machine <project_name>_default_1421225546476_39710.
> VT-x is disabled in the BIOS. (VERR_VMX_MSR_VMXON_DISABLED).

Investigamos como resolver este problema y fácilmente encontramos la [respuesta](http://stackoverflow.com/questions/20647610/verr-vmx-msr-vmxon-disabled-when-starting-an-image-from-oracle-virtual-box) que nos indica que hay que modificar la BIOS para activar la virtualización en nuestro ordenador:

Buscamos como abrir la BIOS en nuestro ordenador, por ejemplo para el HP z220 workstation es pulsando ESC y luego F10 tal y como comentan [aquí](http://h30434.www3.hp.com/t5/Desktop-Hardware/How-do-I-enter-BIOS-on-Z220-workstation-what-is-the-exact/td-p/2437513)

Si no esta muy claro donde esta la opción donde activar la virtualización lo buscamos y rápidamente encontraremos cosas como [esta](http://h30434.www3.hp.com/t5/Desktop-Hardware/z220-hyper-v/td-p/2178695)

Reiniciamos y volvemos a ejecutar `vagrant up`

### Habilitar sistema de ficheros NFS (solo en Ubuntu)

Si al hacer `vagrant up` te sale esto:

> Bringing machine 'default' up with 'virtualbox' provider...
> ==> default: Checking if box 'ubuntu/trusty64' is up to date...
> It appears your machine doesn't support NFS, or there is not an
> adapter to enable NFS on this machine for Vagrant. Please verify
> that `nfsd` is installed on your machine, and try again. If you're
> on Windows, NFS isn't supported. If the problem persists, please
> contact Vagrant support.

Hay que instalar soporte para ntfs

```bash
$ sudo apt-get install nfs-common nfs-kernel-server
```

Después de esto volvemos a ejecutar `vagrant up`

> ==> default: Notice: Finished catalog run in 257.51 seconds

Y ya tenemos nuestra máquina virtual funcionando

### Configurar los hosts

Antes de poder empezar a ustilizar el proyecto, hay que configurar los hosts

Editar el fichero `/etc/hosts` y añadir las líneas

```bash
10.0.0.12       loc.websockets.com
```

### Problemas conocidos:

* Ubuntu
  * No funciona con sistemas de cifrado "ncryptfs"
  * Necesita instalar soporte para "nfs"
* Windows
  * Como no soporta sistema de ficheros "nfs" se ignora esta opción
  