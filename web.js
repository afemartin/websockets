var express = require('express');
var http = require('http');

var app = express();

app.set('view engine', 'jade');

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {
    res.render('index', { title: 'Hey', message: 'Hello there!'});
});

http.createServer(app).listen(8000, function(){
    console.log('Express server listening on port 8000');
});
